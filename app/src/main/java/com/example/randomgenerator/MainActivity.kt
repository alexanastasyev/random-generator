package com.example.randomgenerator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate

class MainActivity : AppCompatActivity() {

    private lateinit var editTextFrom: EditText;
    private lateinit var editTextTo: EditText;
    private lateinit var textViewNumber: TextView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        editTextFrom = findViewById(R.id.editTextNumberDecimalFrom)
        editTextTo = findViewById(R.id.editTextNumberDecimalTo)
        textViewNumber = findViewById(R.id.textViewNumber)

        if (savedInstanceState != null) {
            editTextFrom.setText(savedInstanceState.getString("min", getString(R.string.zero)))
            editTextTo.setText(savedInstanceState.getString("max", getString(R.string.ten)))
            textViewNumber.text = savedInstanceState.getString("value", getString(R.string.zero))
        }

    }

    fun onClickGenerate(view: View) {
        try {
            val min = editTextFrom.text.toString().toInt()
            val max = editTextTo.text.toString().toInt()

            if (max < min) {
                Toast.makeText(this, getString(R.string.warn_max_less_then_min), Toast.LENGTH_SHORT).show()
                return
            }

            val number = ((Math.random() * (max - min + 1)) + min).toInt()
            textViewNumber.text = number.toString()

        } catch (e: Exception) {
            Toast.makeText(this, getString(R.string.warn_numbers_not_integer), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("min", editTextFrom.text.toString())
        outState.putString("max", editTextTo.text.toString())
        outState.putString("value", textViewNumber.text.toString())
    }

}